﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace aeroflot
{
    public class Flight
    {
        public string GoalType { get; set; }            //пассажирский или грузоперевозки
        public string TimeType { get; set; }            //регулярный или разовый
        public int Cargo { get; set; }                   //сколько нужно перевезти груза(людей)
        public City DepartureCity { get; set; }         //город отправления
        public City DestinationCity { get; set; }       //город назначения
        public DayOfWeek WeekDay { get; set; }          //день недели отправления
        public int Hour { get; set; }                    //час отправления
        public int Length { get; set; }                 //длительность рейса
        public int LivingTime { get; set; }             //срок годности рейса
        public int TimeInList { get; set; }               //время нахождения в списке
        public int Costs { get; set; }                   //расходы на рейс

        public int Dohod { get; set; }                  //доход от рейса

        

        public Flight()
        {

        }

        
    }

    
}
