﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace aeroflot
{
    [Serializable]
    public class Plane
    {
        public int Id { get; set; }
        public bool IsRent { get; set; }
        [XmlAttribute("name")]
        public string Name { get; set; }
        public int Iznos { get; set; }
        public int Price { get; set; }
        public int RentDays { get; set; }
        public TypePlane Type;
        public List<Flight> Flights;
        public int CurrentCity { get; set; }
        public bool OnFlight { get; set; }

        public Plane()
        { }

        public Plane(int _id, bool _isRent, string _name, int _iznos, int _price, int _rentDays, TypePlane _type)
        {
            Flights = new List<Flight>();
            Id = _id;
            IsRent = _isRent;
            Name = _name;
            Iznos = _iznos;
            Price = _price;
            RentDays = _rentDays;
            Type = _type;
        }
    }

    
}
