﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace aeroflot
{
    [Serializable]
    public class TypePlane
    {

        [XmlAttribute("name")]
        public string NameType { get; set; }
        public int Price { get; set; }
        public int RentPrice { get; set; }
        public int Length { get; set; }
        public int Weight { get; set; }
        public int Servise { get; set; }

        public TypePlane(string _name, int _price, int _rentPrice, int _length, int _weight, int _servise)
        {
            NameType = _name;
            Price = _price;
            RentPrice = _rentPrice;
            Length = _length;
            Weight = _weight;
            Servise = _servise;
        }
        public TypePlane()
        { }
    }
}
