﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aeroflot
{
    public static class Functions
    {
        public static int GetRandomNum(int _maxValue = 1, int _minValue = 0)  //генерация рандомного числа
        {
            if (_minValue > _maxValue)
                return 0;
            else
            {
                if (_maxValue == _minValue)
                    return _minValue;
                else
                {
                    System.Security.Cryptography.RNGCryptoServiceProvider rnd = new System.Security.Cryptography.RNGCryptoServiceProvider();
                    byte[] randombyte = new byte[1]; //Получаем наш случайный байт
                    rnd.GetBytes(randombyte);
                    double random_multiplyer = (randombyte[0] / 255d); //превращаем его в число от 0 до 1
                    int difference = _maxValue - _minValue; //получаем разницу между минимальным и максимальным значением 
                    return (int)(_minValue + Math.Round(random_multiplyer * difference)); //возвращаем сумму минимального значения и числа от 0 до difference
                }
            }
        }

        
    }
}
