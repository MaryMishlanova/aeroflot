﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;

namespace aeroflot
{
    public partial class MainForm : Form
    {
        ///////////////////////////////////////////
        //// глобальные переменные и константы ////
        ///////////////////////////////////////////
        private const int MinLength = 1;  //часы полета
        private const int MaxLength = 15;
        private const int MinWeight = 10;  //вес(кол-во людей)
        private const int MaxWeight = 200;
        private const int MaxRentDays = 7;
        private bool FlagUpdate = false;
        private bool FlagDelete = false;
        private bool Konkurent = false;
        private const int MinFuelPrice = 5;  //цена за литр топлива
        private const int MaxFuelPrice = 10;

        public const int FuelPerFlightHour = 10; //сколько топлива тратится за час полета (л)

        private List<BasicPlane> BasicPlanes;
        private List<Plane> AvaliablePlanes; //самолеты в магазине
        public List<Plane> MyPlanes; //мои самолеты
        private List<Flight> AllFlights;
        public List<Flight> ChoosenFlights;
        public List<City> Cities;
        public int[,] DistanseMatrix; //матрица часов полета

        private int CompanyImage;
        public int FuelPrice;

        public MainForm()
        {
            InitializeComponent();
            
        }
        /////////////////////////////////
        //// вспомогательные функции ////
        /////////////////////////////////
        public static int[,] GetDistanse(int _n) //формирование матрицы расстояний
        {
            int[,] result = new int[_n, _n];
            for (int i = 0; i < _n; i++)
            {
                for (int j = i; j < _n; j++)
                {
                    if (i == j)
                        result[i, j] = 0;
                    else
                    {
                        result[i, j] = Functions.GetRandomNum(MaxLength, MinLength);
                        result[j, i] = result[i, j];
                    }
                }
            }
            return result;
        }

        private void Neustoika(int _summ)
        {
            MessageBox.Show("Рейс не был выполнен, с Вас списана неустойка");
            TextMoney.Text = (Int32.Parse(TextMoney.Text) - _summ).ToString();
        }

        private bool FlightStart(int _i, int _j)
        {
            if (Int32.Parse(TextMoney.Text) < MyPlanes[_i].Flights[_j].Costs)
            {
                MessageBox.Show(string.Format("У вас недостаточно средств, чтобы совершить полет из {0} в {1} (расходы на полет: ${2})",
                    MyPlanes[_i].Flights[_j].DepartureCity.Name,
                    MyPlanes[_i].Flights[_j].DestinationCity.Name,
                    MyPlanes[_i].Flights[_j].Costs.ToString()));
                return false;
            }
            else
            {
                if (Cities[MyPlanes[_i].CurrentCity].Name != MyPlanes[_i].Flights[_j].DepartureCity.Name)
                {
                    MessageBox.Show("Ваш самолет не в том городе");
                    return false;
                }
                else
                {
                    MyPlanes[_i].OnFlight = true;
                    TextMoney.Text = (Int32.Parse(TextMoney.Text) - MyPlanes[_i].Flights[_j].Costs).ToString(); //вычитаются расходы на полет
                    return true;
                }
            }
        }

        //вызать эту функцию когда самолет приземляется
        private void FlightFinish(int _i, int _j)
        {
            AddIznos(_i, _j);
            TextMoney.Text = (Int32.Parse(TextMoney.Text) + MyPlanes[_i].Flights[_j].Dohod).ToString(); //прибавляется доход от полета
            MyPlanes[_i].CurrentCity = Cities.FindIndex(x => x.Name == MyPlanes[_i].Flights[_j].DestinationCity.Name);
            if (MyPlanes[_i].Flights[_j].GoalType == "Regular")
            {
                MyPlanes[_i].Flights[_j].Hour = 168;
            }
            else
            {
                MyPlanes[_i].Flights.RemoveAt(_j);
            }
            
            MyPlanes[_i].OnFlight = false;
        }
        //функция, добавляющая самолету износ после полета
        
        private void AddIznos(int _i, int _j)
        {
            int iznnosDelta = MyPlanes[_i].Flights[_j].Length; //прибавляем количество часов полета

            if (MyPlanes[_i].Iznos < 100 && MyPlanes[_i].Iznos + iznnosDelta >= 100)
            {
                SetCompanyImage(CompanyImage - 5);
            }

            MyPlanes[_i].Iznos += iznnosDelta;
            MyPlanes[_i].Price -= iznnosDelta * 100;
            if (MyPlanes[_i].Price < 0)
            {
                MyPlanes[_i].Price = 0;
            }
            GridMyPlanesRefresh(); 
        }

        private void SetFuelPrice()
        {
            FuelPrice = Functions.GetRandomNum(MaxFuelPrice, MinFuelPrice);
            lblFuelPrice.Text = "$" + FuelPrice.ToString();
        }

        //обновление срока аренды для арендованных самолетов
        private void RentPlanesDayChange()
        {
            foreach (Plane plane in MyPlanes)
            {
                if (plane.IsRent)
                {
                    plane.RentDays--;
                    if (plane.RentDays < 0)
                    {
                        MyPlanes.Remove(plane);
                    }
                }
            }
            GridMyPlanesRefresh();
        }

        private void SetCompanyImage(int image)
        {
            int porog = 50;
            int deltaPas = 0;
            if (CompanyImage < porog && image >= porog)
            {
                deltaPas = 5;
            }
            if (CompanyImage >= porog && image < porog)
            {
                deltaPas = -5;
            }
            CompanyImage = image;
            if (CompanyImage < 0)
            {
                CompanyImage = 0;
            }
            if (CompanyImage > 100)
            {
                CompanyImage = 100;
            }
            lblImage.Text = CompanyImage.ToString() + "%";

            if (deltaPas != 0)
            {
                foreach (Flight flight in AllFlights)
                {
                    double priceForOnePas = (double)flight.Costs / flight.Cargo;
                    flight.Cargo += deltaPas;
                    flight.Costs += (int)(deltaPas * priceForOnePas);
                }

                DrawFlightTable();
                GridAvaliableFlights_CellClick(null, null);
            }
        }

        private Flight FlightGenerate()   //генерация одного случайного рейса
        {
            Flight result = new Flight();
            int temp = Functions.GetRandomNum();
            if (temp == 0)
                result.GoalType = "Cargo";
            else
                result.GoalType = "Passengers";
            temp = Functions.GetRandomNum();
            if (temp == 0)
                result.TimeType = "Regular";
            else
                result.TimeType = "One time";
            result.Cargo = Functions.GetRandomNum(MaxWeight, MinWeight);
            int city = Functions.GetRandomNum(Cities.Count - 1);
            result.DepartureCity = Cities[city];
            do
            {
                temp = Functions.GetRandomNum(Cities.Count - 1);
            }
            while (city == temp);
            result.DestinationCity = Cities[temp];
            result.TimeInList = Functions.GetRandomNum(23);
            result.LivingTime = Functions.GetRandomNum(47, 24);
            result.Hour = -1000;
            result.Length = DistanseMatrix[city, temp];
            result.Dohod = Functions.GetRandomNum(300, 100) * result.Cargo * result.Length;
            result.Costs = FuelPrice * FuelPerFlightHour * result.Length;
            return result;
        }

        public List<Flight> GenerateListFlights(int _n)  //Генерация списка рейсов
        {
            List<Flight> result = new List<Flight>();
            for (int i = 0; i < _n; i++)
                result.Add(FlightGenerate());
            return result;
        }

        public List<Flight> ConcatenateLists(List<Flight> _list1, List<Flight> _list2) //объединение 2 списков
        {
            List<Flight> result = _list1;
            for (int i = 0; i < _list2.Count(); i++)
                result.Add(_list2[i]);
            return result;
        }

        public List<Plane> FormatListPlane(List<BasicPlane> _allPlane)
        {
            List<Plane> result = new List<Plane>(); // генерируемый список
            int kolPlanes = Functions.GetRandomNum(15, 5);
            for (int i = 0; i < kolPlanes; i++) //количество генерируемых самолетов 
            {
                int rndPlane = Functions.GetRandomNum(_allPlane.Count - 1);
                int rndType = Functions.GetRandomNum(_allPlane[rndPlane].Type.Count - 1);
                int rndRentDays = Functions.GetRandomNum(MaxRentDays - 1) + 1;

                result.Add(new Plane(i, false, _allPlane[rndPlane].Name, 0, _allPlane[rndPlane].Type[rndType].Price, rndRentDays, _allPlane[rndPlane].Type[rndType]));
            }
            return result;
        }
        //////////////////////////
        //// отрисовка таблиц ////
        //////////////////////////
        public void RemovDublicate() // скрываем дублирующиемя строки в магазине
        {
            for (int intI = 0; intI < GridAvaliablePlanes.Rows.Count; intI++)
            {
                for (int intJ = intI + 1; intJ < GridAvaliablePlanes.RowCount; intJ++)
                {
                    if ((GridAvaliablePlanes.Rows[intJ].Cells[0].Value.ToString() == GridAvaliablePlanes.Rows[intI].Cells[0].Value.ToString())
                                && (GridAvaliablePlanes.Rows[intJ].Cells[1].Value.ToString() == GridAvaliablePlanes.Rows[intI].Cells[1].Value.ToString()))
                        GridAvaliablePlanes.Rows.RemoveAt(intJ);
                }
            }
        }

        private void DrawPlaneTable()
        {
            GridAvaliablePlanes.Rows.Clear();
            if (GridAvaliablePlanes.ColumnCount == 0)
            {
                GridAvaliablePlanes.Columns.Add("Name", "Name");
                GridAvaliablePlanes.Columns.Add("Type", "Type");
            }
            for (int i = 0; i < AvaliablePlanes.Count; i++)
            {
                GridAvaliablePlanes.Rows.Add();
                GridAvaliablePlanes.Rows[i].Cells[0].Value = AvaliablePlanes[i].Name;
                GridAvaliablePlanes.Rows[i].Cells[1].Value = AvaliablePlanes[i].Type.NameType;
            }
        }

        private void DrawFlightTable()
        {
            GridAvaliableFlights.Rows.Clear();
            if (GridAvaliableFlights.ColumnCount == 0)
            {
                GridAvaliableFlights.Columns.Add("ID", "ID");
                GridAvaliableFlights.Columns.Add("Departure", "Departure");
                GridAvaliableFlights.Columns.Add("Destination", "Destination");
            }
            for (int i = 0; i < AllFlights.Count; i++)
            {
                GridAvaliableFlights.Rows.Add(i.ToString(), AllFlights[i].DepartureCity.Name, AllFlights[i].DestinationCity.Name);
            }
        }

        public void GridMyPlanesRefresh()
        {
            GridMyPlanes.Rows.Clear();
            foreach (Plane plane in MyPlanes)
            {
                GridMyPlanes.Rows.Add(plane.Name,
                    plane.Type.NameType,
                    plane.Price,
                    plane.IsRent ? plane.RentDays.ToString() : " - ",
                    plane.Id);
            }
        }
        ///////////////////////////
        //// обработка событий ////
        ///////////////////////////
        private void GridMyPlanes_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            TextPlaneFlight.Clear();
            foreach (var i in MyPlanes[GridMyPlanes.CurrentRow.Index].Flights)
            {
                TextPlaneFlight.Text += i.DepartureCity.Name + " " + i.DestinationCity.Name + " " + i.GoalType + Environment.NewLine;
            }
            BtnOpenFlightList.Enabled = true;
            BtnSellPlane.Enabled = true;
        } 

        private void GridAvaliablePlanes_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (!FlagUpdate)
            {
                GridPlaneInfo.Rows.Clear();
                int indexGridPlane = GridAvaliablePlanes.CurrentRow.Index; //то, что выделили
                if (indexGridPlane >= 0)
                {
                    foreach (var i in AvaliablePlanes)
                    {
                        if (i.Name == GridAvaliablePlanes.Rows[indexGridPlane].Cells[0].Value.ToString() &&
                                    i.Type.NameType == GridAvaliablePlanes.Rows[indexGridPlane].Cells[1].Value.ToString())
                            GridPlaneInfo.Rows.Add(i.Price, i.Type.Length, i.Type.Weight, i.Type.Servise, i.RentDays, i.RentDays * i.Type.RentPrice, i.Id);
                    }
                }
            }
        }

        private void GridAvaliableFlights_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!FlagDelete)
            {
                if (GridAvaliableFlights.CurrentRow != null)
                {
                    int CurFlight_Id = GridAvaliableFlights.CurrentRow.Index;
                    if (GridFlightInfo.Columns.Count == 0)
                    {
                        GridFlightInfo.Columns.Add("ID", "ID");
                        GridFlightInfo.Columns.Add("GoalType", "GoalType");
                        GridFlightInfo.Columns.Add("TimeType", "TimeType");
                        GridFlightInfo.Columns.Add("TimeInList", "TimeInList");
                        GridFlightInfo.Columns.Add("Cargo", "Cargo");
                        GridFlightInfo.Columns.Add("Day of week", "Day of week");
                        GridFlightInfo.Columns.Add("Hour", "Hour");
                        GridFlightInfo.Columns.Add("Costs", "Costs");
                    }
                    GridFlightInfo.Rows[0].Cells[0].Value = CurFlight_Id;
                    GridFlightInfo.Rows[0].Cells[1].Value = AllFlights[CurFlight_Id].GoalType;
                    GridFlightInfo.Rows[0].Cells[2].Value = AllFlights[CurFlight_Id].TimeType;
                    GridFlightInfo.Rows[0].Cells[3].Value = AllFlights[CurFlight_Id].TimeInList;
                    GridFlightInfo.Rows[0].Cells[4].Value = AllFlights[CurFlight_Id].Cargo;
                    GridFlightInfo.Rows[0].Cells[5].Value = AllFlights[CurFlight_Id].WeekDay;
                    GridFlightInfo.Rows[0].Cells[6].Value = AllFlights[CurFlight_Id].Hour;
                    GridFlightInfo.Rows[0].Cells[7].Value = AllFlights[CurFlight_Id].Costs;
                }
            }
        }
        /////////////////////////////////////
        //// обработка нажатий на кнопки ////
        /////////////////////////////////////
        private void BtnRun_Click(object sender, EventArgs e)
        {
            GameTime.Interval = 60000;
            GameTime.Enabled = true;

            BtnRun.Enabled = false;
            BtnPause.Enabled = true;
            BtnXRun.Enabled = true;
            BtnXXRun.Enabled = true;
        }

        private void BtnPause_Click(object sender, EventArgs e)
        {
            GameTime.Enabled = false;
            
            BtnRun.Enabled = true;
            BtnPause.Enabled = false;
            BtnXRun.Enabled = true;
            BtnXXRun.Enabled = true;
        }

        private void BtnXRun_Click(object sender, EventArgs e)
        {
            GameTime.Interval = 1000;
            GameTime.Enabled = true;

            BtnRun.Enabled = true;
            BtnPause.Enabled = true;
            BtnXRun.Enabled = false;
            BtnXXRun.Enabled = true;
        }

        private void BtnXXRun_Click(object sender, EventArgs e)
        {
            GameTime.Interval = 10;
            GameTime.Enabled = true;

            BtnRun.Enabled = true;
            BtnPause.Enabled = true;
            BtnXRun.Enabled = true;
            BtnXXRun.Enabled = false;
        }

        private void BtnStartGame_Click(object sender, EventArgs e)
        {
            BtnRun.Enabled = true;
            BtnPause.Enabled = false;
            BtnXRun.Enabled = true;
            BtnXXRun.Enabled = true;
            BtnRestart.Enabled = true;
            BtnExit.Enabled = true;
            BtnStartGame.Enabled = false;
            MyCompanyPanel.Enabled = true;
            FlagUpdate = true;
            FlagDelete = true;
            

            /////////////////////////////////////////////
            //// инициализация глобальных переменных ////
            /////////////////////////////////////////////
            SetFuelPrice();
            MyPlanes = new List<Plane>();
            XmlSerializer formatter = new XmlSerializer(typeof(List<BasicPlane>));
            using (FileStream fs = new FileStream("plane.xml", FileMode.OpenOrCreate))
            {
                BasicPlanes = (List<BasicPlane>)formatter.Deserialize(fs);
            }
            AvaliablePlanes = FormatListPlane(BasicPlanes);
            DrawPlaneTable();
            RemovDublicate();

            ChoosenFlights = new List<Flight>();
            formatter = new XmlSerializer(typeof(List<City>));
            using (FileStream fs = new FileStream("cities.xml", FileMode.OpenOrCreate))
            {
                Cities = (List<City>)formatter.Deserialize(fs);
            }
            DistanseMatrix = GetDistanse(Cities.Count);
            AllFlights = GenerateListFlights(Functions.GetRandomNum(10, 5));
            DrawFlightTable();

            TextMoney.Text = "1000000";
            SetCompanyImage(70);

            FlagDelete = false;
            FlagUpdate = false;
        }

        private void BtnRestart_Click(object sender, EventArgs e)
        {
            BtnRun.Enabled = true;
            BtnPause.Enabled = false;
            BtnXRun.Enabled = true;
            BtnXXRun.Enabled = true;
            BtnExit_Click(sender, e);
            BtnStartGame_Click(sender, e);
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            BtnRun.Enabled = false;
            BtnPause.Enabled = false;
            BtnXRun.Enabled = false;
            BtnXXRun.Enabled = false;
            BtnRestart.Enabled = false;
            BtnExit.Enabled = false;
            BtnStartGame.Enabled = true;
            MyCompanyPanel.Enabled = false;
            GameTime.Stop();
            TextDays.Text = "00";
            TextHours.Text = "00";
            TextMinutes.Text = "00";
            ShopPanel.Enabled = false;
            FlightPanel.Enabled = false;
            GridAvaliableFlights.Rows.Clear();
            GridAvaliablePlanes.Rows.Clear();
            GridMyPlanes.Rows.Clear();
        }

        

        private void BtnBuyPlane_Click(object sender, EventArgs e)
        {
            if (GridAvaliablePlanes.Rows.Count == 0) return;
            int idPlaneInfo = GridPlaneInfo.CurrentRow.Index;
            int idPlane = int.Parse(GridPlaneInfo.Rows[idPlaneInfo].Cells["idPlane"].Value.ToString());
            
            Plane newPlane = new Plane();
            
            int idAvaliablePlane = 0;
            for (int i = 0; i < AvaliablePlanes.Count; i++)
                if (AvaliablePlanes[i].Id == idPlane) idAvaliablePlane = i;
            if (AvaliablePlanes[idAvaliablePlane].Price > Int32.Parse(TextMoney.Text))
                MessageBox.Show("Не хватает денег");
            else
            {
                TextMoney.Text = (Int32.Parse(TextMoney.Text) - AvaliablePlanes[idAvaliablePlane].Price).ToString();
                newPlane = AvaliablePlanes[idAvaliablePlane];
                newPlane.CurrentCity = 0;
                newPlane.OnFlight = false;
                MyPlanes.Add(newPlane);
                GridMyPlanesRefresh();
                AvaliablePlanes.RemoveAt(idAvaliablePlane);
                GridPlaneInfo.Rows.RemoveAt(idPlaneInfo);
                if (GridPlaneInfo.Rows.Count == 0) //если последний самолет такого типа
                    GridAvaliablePlanes.Rows.RemoveAt(GridAvaliablePlanes.CurrentRow.Index);
                ShopPanel.Enabled = false;
                BtnSelectFlight.Enabled = true;
                BtnRun_Click(sender, e);
            }
        }

        private void BtnSelectFlight_Click(object sender, EventArgs e)
        {
            int CurFlight_Id = GridAvaliableFlights.CurrentRow.Index; //получаем id выбранного рейса
            ChoosenFlights.Add(AllFlights[CurFlight_Id]);  //переносим в список выбранных рейсов
            AllFlights.RemoveAt(CurFlight_Id);
            DrawFlightTable();
            FlightPanel.Enabled = false;
            BtnOpenPlaneShop.Enabled = true;
            BtnSellPlane.Enabled = true;
            BtnReturnPlane.Enabled = true;
            BtnRun_Click(sender, e);
        }

        private void BtnCanselSelect_Click(object sender, EventArgs e)
        {
            FlightPanel.Enabled = false;
            BtnOpenPlaneShop.Enabled = true;
            BtnSellPlane.Enabled = true;
            BtnReturnPlane.Enabled = true;
            BtnRun_Click(sender, e);
        }

        private void BtnLeasePlane_Click(object sender, EventArgs e)
        {
            if (GridAvaliablePlanes.Rows.Count == 0) return;
            int idPlaneInfo = GridPlaneInfo.CurrentRow.Index;
            int idPlane = int.Parse(GridPlaneInfo.Rows[idPlaneInfo].Cells["idPlane"].Value.ToString());

            int idAvaliablePlane = 0;
            for (int i = 0; i < AvaliablePlanes.Count; i++)
                if (AvaliablePlanes[i].Id == idPlane) idAvaliablePlane = i;
            if (AvaliablePlanes[idAvaliablePlane].RentDays <= 0)
            {
                MessageBox.Show("Данный самолет недоступен для аренды");
                return;
            }
            int price = AvaliablePlanes[idAvaliablePlane].RentDays * AvaliablePlanes[idAvaliablePlane].Type.RentPrice;
            if (price > Int32.Parse(TextMoney.Text))
                MessageBox.Show("Не хватает денег");
            else
            {
                TextMoney.Text = (Int32.Parse(TextMoney.Text) - price).ToString();
                Plane newPlane = AvaliablePlanes[idAvaliablePlane];
                newPlane.CurrentCity = 0;
                newPlane.IsRent = true;
                newPlane.OnFlight = false;
                MyPlanes.Add(newPlane);
                GridMyPlanesRefresh();
                AvaliablePlanes.RemoveAt(idAvaliablePlane);
                GridPlaneInfo.Rows.RemoveAt(idPlaneInfo);
                if (GridPlaneInfo.Rows.Count == 0) //если последний самолет такого типа
                    GridAvaliablePlanes.Rows.RemoveAt(GridAvaliablePlanes.CurrentRow.Index);
                ShopPanel.Enabled = false;
                BtnSelectFlight.Enabled = true;
                BtnRun_Click(sender, e);
            }
        }

        private void BtnReturnPlane_Click(object sender, EventArgs e)
        {
            if (GridMyPlanes.Rows.Count == 0) return;
            Plane returnPlane = new Plane();

            int idMyPlane = GridMyPlanes.CurrentRow.Index;
            int idReturnPlane = 0;
            int idPlane = int.Parse(GridMyPlanes.Rows[idMyPlane].Cells["idMyPlane"].Value.ToString());
            for (int i = 0; i < MyPlanes.Count; i++)
                if (MyPlanes[i].Id == idPlane) idReturnPlane = i;
            if (MyPlanes[idReturnPlane].OnFlight)
            {
                MessageBox.Show("Самолет в полете, вы не можете его продать");
            }
            else
            {
                returnPlane = MyPlanes[idReturnPlane];

                if (!returnPlane.IsRent)
                {
                    MessageBox.Show("Данный самолет не был арендован");
                    return;
                }
                returnPlane.RentDays--;
                if (returnPlane.RentDays < 0)
                {
                    returnPlane.RentDays = 0;
                }

                int price = returnPlane.RentDays * returnPlane.Type.RentPrice;
                price -= (int)(price * 0.1);
                TextMoney.Text = (Int32.Parse(TextMoney.Text) + price).ToString();

                returnPlane.IsRent = false;
                AvaliablePlanes.Add(returnPlane);

                GridAvaliablePlanes.Rows.Add(returnPlane.Name,
                                             returnPlane.Type.NameType);

                int idGridAvaliablePlanes = GridAvaliablePlanes.CurrentRow.Index;//какой элемент выделен

                if (GridAvaliablePlanes.Rows[idGridAvaliablePlanes].Cells[0].Value.ToString() == returnPlane.Name &&
                    GridAvaliablePlanes.Rows[idGridAvaliablePlanes].Cells[1].Value.ToString() == returnPlane.Type.NameType)
                    GridPlaneInfo.Rows.Add(returnPlane.Price,
                                           returnPlane.Type.Length,
                                           returnPlane.Type.Weight,
                                           returnPlane.Type.Servise,
                                           returnPlane.RentDays,
                                           returnPlane.RentDays * returnPlane.Type.RentPrice,
                                           returnPlane.Id);
                ChoosenFlights = ConcatenateLists(ChoosenFlights, MyPlanes[idReturnPlane].Flights);
                MyPlanes.RemoveAt(idReturnPlane);
                GridMyPlanes.Rows.RemoveAt(idMyPlane);
                RemovDublicate();
            }
        }

        private void BtnSellPlane_Click(object sender, EventArgs e)
        {
            if (GridMyPlanes.Rows.Count == 0) return;
            int idMyPlane = GridMyPlanes.CurrentRow.Index;
            int idPlane = int.Parse(GridMyPlanes.Rows[idMyPlane].Cells["idMyPlane"].Value.ToString());
            Plane returnPlane = new Plane();

            int idReturnPlane = 0;
            for (int i = 0; i < MyPlanes.Count; i++)
                if (MyPlanes[i].Id == idPlane) idReturnPlane = i;
            if (MyPlanes[idReturnPlane].OnFlight)
            {
                MessageBox.Show("Самолет в полете, вы не можете его продать");
            }
            else
            {
                returnPlane = MyPlanes[idReturnPlane];

                if (returnPlane.IsRent)
                {
                    MessageBox.Show("Данный самолет недоступен к продаже");
                    return;
                }

                TextMoney.Text = (Int32.Parse(TextMoney.Text) + returnPlane.Price).ToString();

                AvaliablePlanes.Add(returnPlane);

                GridAvaliablePlanes.Rows.Add(returnPlane.Name,
                                             returnPlane.Type.NameType);

                int idGridAvaliablePlanes = GridAvaliablePlanes.CurrentRow.Index;//какой элемент выделен

                if (GridAvaliablePlanes.Rows[idGridAvaliablePlanes].Cells[0].Value.ToString() == returnPlane.Name &&
                    GridAvaliablePlanes.Rows[idGridAvaliablePlanes].Cells[1].Value.ToString() == returnPlane.Type.NameType)
                    GridPlaneInfo.Rows.Add(returnPlane.Price,
                                           returnPlane.Type.Length,
                                           returnPlane.Type.Weight,
                                           returnPlane.Type.Servise,
                                           returnPlane.RentDays,
                                           returnPlane.RentDays * returnPlane.Type.RentPrice,
                                           returnPlane.Id);
                ChoosenFlights = ConcatenateLists(ChoosenFlights, MyPlanes[idReturnPlane].Flights);
                MyPlanes.RemoveAt(idReturnPlane);
                GridMyPlanes.Rows.RemoveAt(idMyPlane);
                RemovDublicate();
                if (GridMyPlanes.Rows.Count == 0)
                    BtnSellPlane.Enabled = false;
            }
        }



        private void BtnOpenPlaneShop_Click(object sender, EventArgs e)
        {
            ShopPanel.Enabled = true;
            BtnOpenFlightList.Enabled = false;
            BtnPause_Click(sender, e);
            BtnRun.Enabled = false;
            BtnXXRun.Enabled = false;
            BtnXRun.Enabled = false;
        }

        private void BtnCanselBuy_Click(object sender, EventArgs e)
        {
            ShopPanel.Enabled = false;
            BtnSelectFlight.Enabled = true;
            BtnRun_Click(sender, e);
        }

        private void BtnReklama_Click(object sender, EventArgs e)
        {
            int reklamaPrice = 500;
            if (CompanyImage < 100)
            {
                if (Int32.Parse(TextMoney.Text) < reklamaPrice)
                {
                    MessageBox.Show("Не хватает денег");
                }
                else
                {
                    TextMoney.Text = (Int32.Parse(TextMoney.Text) - reklamaPrice).ToString();
                    SetCompanyImage(CompanyImage + 1);
                }
            }
        }

        private void BtnShowTimetable_Click(object sender, EventArgs e)
        {
            Timetable timetable = new Timetable(this);
            GameTime.Stop();
            this.Enabled = false;
            timetable.lbCity.Text = Cities[MyPlanes[GridMyPlanes.CurrentRow.Index].CurrentCity].Name;
            timetable.Show();
        }

        private void BtnOpenFlightList_Click(object sender, EventArgs e)
        {
            FlightPanel.Enabled = true;
            BtnOpenPlaneShop.Enabled = false;
            BtnReturnPlane.Enabled = false;
            BtnSellPlane.Enabled = false;
            BtnPause_Click(sender, e);
        }
        ///////////////////////////
        //// обработка времени ////
        ///////////////////////////
        private void GameTime_Tick(object sender, EventArgs e)
        {
            int minute = Int32.Parse(TextMinutes.Text) + 1;
            if (minute == 60)
            {
                minute = 0;
                int hour = Int32.Parse(TextHours.Text) + 1;
                if (hour == 24)
                {
                    hour = 0;
                    TextDays.Text = String.Format("{0,2:D2}", Int32.Parse(TextDays.Text) + 1); ;
                }
                TextHours.Text = String.Format("{0,2:D2}", hour);
            }
            TextMinutes.Text = String.Format("{0,2:D2}", minute);

            ////////////////////////////////////
            //// проверка начала нового дня ////
            ////////////////////////////////////
            if ((TextHours.Text == "00") && (TextMinutes.Text == "00"))
            {
                //события смены дня
                Konkurent = false;
                SetFuelPrice();
                FlagUpdate = true;
                AvaliablePlanes = FormatListPlane(BasicPlanes);
                DrawPlaneTable();
                RemovDublicate();
                FlagUpdate = false;
                AllFlights = ConcatenateLists(AllFlights, GenerateListFlights(Functions.GetRandomNum(10, 5)));
                DrawFlightTable();
                RentPlanesDayChange();
            }
            /////////////////////////////////////
            //// проверка начала нового часа ////
            /////////////////////////////////////
            if (TextMinutes.Text == "00")
            {
                FlagDelete = true;
                for (int i = 0; i < AllFlights.Count(); i++)
                {
                    if (AllFlights[i].TimeInList == 0)
                    {
                        AllFlights.RemoveAt(i);
                        i--;
                    }
                    else
                    {
                        AllFlights[i].TimeInList--;
                        AllFlights[i].LivingTime--;
                    }
                }

                if (!Konkurent && Functions.GetRandomNum(1, 0) == 1)
                {
                    int flightNum = Functions.GetRandomNum(AllFlights.Count - 1, 0);
                    Flight flight = AllFlights[flightNum];
                    AllFlights.Remove(flight);
                    Konkurent = true;
                    
                    MessageBox.Show(string.Format("Конкурент забрал рейс из {0} в {1} стоимостью {2}", 
                        flight.DepartureCity.Name,
                        flight.DestinationCity.Name,
                        flight.Costs.ToString()));
                }

                DrawFlightTable();
                FlagDelete = false;
                for (int i = 0; i < ChoosenFlights.Count(); i++)
                {
                    if (ChoosenFlights[i].LivingTime == 0)
                    {
                        GameTime.Stop();
                        Neustoika(ChoosenFlights[i].Dohod / 10);
                        ChoosenFlights.RemoveAt(i);
                        i--;
                        GameTime.Start();
                    }
                    else
                    {
                        ChoosenFlights[i].LivingTime--;
                    }
                }
                for (int i = 0; i < MyPlanes.Count(); i++)
                {
                    for (int j = 0; j < MyPlanes[i].Flights.Count(); j++)
                    {
                        
                        ///////////////////////////
                        //// проверка прибытия ////
                        ///////////////////////////
                        if (MyPlanes[i].Flights[j].Hour + MyPlanes[i].Flights[j].Length == 0)
                        {
                            //долетели
                            FlightFinish(i, j);
                            j--;
                        }
                        else
                        {
                            //////////////////////////////
                            //// проверка отправления ////
                            //////////////////////////////
                            if (MyPlanes[i].Flights[j].Hour == 0)
                            {
                                //полетели
                                GameTime.Stop();
                                if (!FlightStart(i, j))
                                {
                                        
                                    Neustoika(MyPlanes[i].Flights[j].Dohod / 10);
                                    MyPlanes[i].Flights.RemoveAt(j);
                                    j--;
                                        
                                }
                                else
                                    MyPlanes[i].Flights[j].Hour--;
                                GameTime.Start();
                            }
                            else
                                MyPlanes[i].Flights[j].Hour--;
                            ////////////////
                            //// прочее ////
                            ////////////////
                               
                        }
                    }
                }
            }
        }
    }
}