﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace aeroflot
{
    [Serializable]
    public class City
    {
        public string Name { get; set; }
        public int PrimeTime { get; set; }  //время начала прайма
        public int PTLength { get; set; }   //длительность прайма
        public int PTCount { get; set; }    //число пассажиров в прайм
        public int PTDcr { get; set; }    //уменьшение числа пассажиров за каждый час от прайма в %

        public City()
        {

        }

        public City(string _name, int _primeTime, int _ptLength, int _ptCount, int _ptDcr)
        {
            Name = _name;
            PrimeTime = _primeTime;
            PTLength = _ptLength;
            PTCount = _ptCount;
            PTDcr = _ptDcr;
        }

    }
}
