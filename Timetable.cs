﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aeroflot
{
    public partial class Timetable : Form
    {
        private MainForm MyForm { get; set; }

        private List<Flight> SFlights;

        private Color SaveColor = Color.White;
        private bool FlagStart = true;
        private int Summ = 0;

        private int CDay, CHour, DayCount;

        public Timetable(MainForm _mainForm)
        {
            InitializeComponent();
            MyForm = _mainForm;
            SFlights = MyForm.MyPlanes[MyForm.GridMyPlanes.CurrentRow.Index].Flights;
            CDay = Int32.Parse(MyForm.TextDays.Text);
            CHour = Int32.Parse(MyForm.TextHours.Text);
            if (CHour == 23)
            {
                CDay++;
                CHour = 0;
                DayCount = 2;
            }
            else
            {
                CHour++;
                DayCount = 3;
            }
            for (int i = 0; i < DayCount; i++)
            {
                ComboDay.Items.Add(CDay + i);
            }
            ComboDay.SelectedIndex = 0;
            DrawCombo();
            DrawFlightTable();
            DrawMyFlightTable();
            DrawTimeTable();
            for (int i = 0; i < MyForm.Cities.Count; i++)
            {
                ComboCityTo.Items.Add(MyForm.Cities[i].Name);
                ComboCityFrom.Items.Add(MyForm.Cities[i].Name);
            }
            ComboCityTo.SelectedIndex = 0;
            FlagStart = false;
        }

        private bool CheckPlane()
        {
            if ((MyForm.MyPlanes[MyForm.GridMyPlanes.CurrentRow.Index].Name == MyForm.ChoosenFlights[GridSelectedFlights.CurrentRow.Index].GoalType)
                && (MyForm.MyPlanes[MyForm.GridMyPlanes.CurrentRow.Index].Type.Weight >= MyForm.ChoosenFlights[GridSelectedFlights.CurrentRow.Index].Cargo)
                && (MyForm.MyPlanes[MyForm.GridMyPlanes.CurrentRow.Index].Type.Length >= MyForm.ChoosenFlights[GridSelectedFlights.CurrentRow.Index].Length))
                return true;
            else
            {
                MessageBox.Show("Самолет не подходит для данного рейса");
                return false;
            }
        }

        private void DrawCombo()
        {
            ComboHour.Items.Clear();
            switch (ComboDay.SelectedIndex)
            {
                case 0:
                    {
                        for (int i = CHour; i < 24; i++)
                        {
                            ComboHour.Items.Add(i);
                        }
                        break;
                    }
                case 1:
                    {
                        for (int i = 0; i < 24; i++)
                        {
                            ComboHour.Items.Add(i);
                        }
                        break;
                    }
                case 2:
                    {
                        for (int i = 0; i < CHour; i++)
                        {
                            ComboHour.Items.Add(i);
                        }
                        break;
                    }
                default: MessageBox.Show("Ошибка"); break;
            }
            ComboHour.SelectedIndex = 0;
        }

        private void DrawFlightTable()
        {
            GridSelectedFlights.Rows.Clear();
            if (GridSelectedFlights.ColumnCount == 0)
            {
                GridSelectedFlights.Columns.Add("ID", "ID");
                GridSelectedFlights.Columns.Add("Departure", "Departure");
                GridSelectedFlights.Columns.Add("Destination", "Destination");
                GridSelectedFlights.Columns.Add("Destination", "Length");
                GridSelectedFlights.Columns.Add("Destination", "Time to go");
            }
            for (int i = 0; i < MyForm.ChoosenFlights.Count; i++)
            {
                GridSelectedFlights.Rows.Add(i.ToString(), 
                    MyForm.ChoosenFlights[i].DepartureCity.Name, 
                    MyForm.ChoosenFlights[i].DestinationCity.Name,
                    MyForm.ChoosenFlights[i].Length,
                    MyForm.ChoosenFlights[i].LivingTime);
            }
        }

        private void DrawMyFlightTable()
        {
            GridChoosedFlights.Rows.Clear();
            if (GridChoosedFlights.ColumnCount == 0)
            {
                GridChoosedFlights.Columns.Add("ID", "ID");
                GridChoosedFlights.Columns.Add("Departure", "Departure");
                GridChoosedFlights.Columns.Add("Destination", "Destination");
                GridChoosedFlights.Columns.Add("Destination", "Length");
                GridChoosedFlights.Columns.Add("Destination", "Time to flight");
                GridChoosedFlights.Columns.Add("Destination", "Type");
            }
            for (int i = 0; i < SFlights.Count; i++)
            {
                GridChoosedFlights.Rows.Add(i.ToString(),
                    SFlights[i].DepartureCity.Name,
                    SFlights[i].DestinationCity.Name,
                    SFlights[i].Length,
                    SFlights[i].Hour,
                    SFlights[i].TimeType);
            }
        }

        private void IncrDate(ref int _day, ref int _hour)
        {
            if (_hour == 23)
            {
                _day++;
                _hour = 0;
            }
            else
            {
                _hour++;
            }
        }

        private void DrawTimeTable()
        {
            GridTime.Rows.Clear();
            for(int i = 0; i < 8; i++)
                GridTime.Rows.Add();
            int tmp = Summ, d = CDay, h = CHour;
            for (int i = 0; i < 48; i++)
            {
                int k = 0, p = 0;
                GetNums(ref k, ref p, i);
                GridTime[k, p].Value = d.ToString() + "-й день, " + h.ToString() + "-й час";
                IncrDate(ref d, ref h);
            }
            
            GridTime[0, 0].Style.SelectionBackColor = Color.White;
            GridTime[0, 0].Style.SelectionForeColor = Color.Black;
            for (int i = 0; i < SFlights.Count; i++)
            {
                if (SFlights[i].Hour < 48)
                {
                    int start = SFlights[i].Hour, end = SFlights[i].Hour + SFlights[i].Length;
                    if (start < 0)
                        start = 0;
                    if (end > 47)
                        end = 47;
                    for (int k = start; k < end; k++)
                    {
                        int q = 0, p = 0;
                        GetNums(ref q, ref p, k);
                        GridTime[q, p].Style.BackColor = Color.Yellow;
                        GridTime[q, p].Style.SelectionBackColor = Color.Yellow;
                    }
                }
            }
            Summ = tmp;
        }

        private void BtnCanselTimetable_Click(object sender, EventArgs e)
        {
            MyForm.GridMyPlanesRefresh();
            this.Close();
        }

        private void Timetable_FormClosed(object sender, FormClosedEventArgs e)
        {
            MyForm.Enabled = true;
            MyForm.MyPlanes[MyForm.GridMyPlanes.CurrentRow.Index].Flights = SFlights;
        }

        private void ComboDay_SelectedIndexChanged(object sender, EventArgs e)
        {
            DrawCombo();
        }

        private void BtnChangeTable_Click(object sender, EventArgs e)
        {
            if (GridChoosedFlights.Enabled)
            {
                GridChoosedFlights.Enabled = false;
                GridSelectedFlights.Enabled = true;
            }
            else
            {
                GridChoosedFlights.Enabled = true;
                GridSelectedFlights.Enabled = false;
            }
        }

        private void GetNums(ref int _i, ref int _j, int _sum = -1)
        {
            if (_sum == -1)
            {
                switch (ComboDay.SelectedIndex)
                {
                    case 0:
                        {
                            _sum = ComboHour.SelectedIndex;
                            break;
                        }
                    case 1:
                        {
                            _sum = ComboHour.SelectedIndex + 24 - CHour;
                            break;
                        }
                    case 2:
                        {
                            _sum = ComboHour.SelectedIndex + 48 - CHour;
                            break;
                        }
                    default: MessageBox.Show("Ошибка"); break;
                }
                Summ = _sum;
            }
            _i = _sum % 6;
            _j = _sum / 6;
        }

        private void ComboHour_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!FlagStart)
            {
                int i = 0, j = 0;
                GetNums(ref i, ref j, Summ);
                GridTime[i, j].Style.BackColor = SaveColor;
                GridTime[i, j].Style.SelectionBackColor = SaveColor;
                GetNums(ref i, ref j);
                SaveColor = GridTime[i, j].Style.BackColor;
                GridTime[i, j].Style.BackColor = Color.Red;
                GridTime[i, j].Style.SelectionBackColor = Color.Red;
            }
        }

        private bool CheckAvaliable(int _length = -1)
        {
            int start = Summ, ind = -1;
            if (_length == -1)
            {
                if (GridSelectedFlights.Enabled)
                {
                    _length = MyForm.ChoosenFlights[GridSelectedFlights.CurrentRow.Index].Length;
                }
                else
                {
                    ind = GridChoosedFlights.CurrentRow.Index;
                    _length = SFlights[ind].Length;
                }
            }
            for (int i = 0; i < SFlights.Count; i++)
            {
                if (i != ind)
                {
                    if ((start >= SFlights[i].Hour)&&(start < SFlights[i].Hour + SFlights[i].Length)
                        ||(start + _length >= SFlights[i].Hour)&&(start + _length < SFlights[i].Hour + SFlights[i].Length)
                        ||(start < SFlights[i].Hour)&&(start + _length >= SFlights[i].Hour + SFlights[i].Length))
                    {
                        MessageBox.Show("Нельзя назначить на это время");
                        return false;
                    }
                }
            }
            return true;
        }

        private void BtnSelectTime_Click(object sender, EventArgs e)
        {
            if (CheckAvaliable())
            {
                if (GridSelectedFlights.Enabled)
                {
                    if (CheckPlane())
                    {
                        MyForm.ChoosenFlights[GridSelectedFlights.CurrentRow.Index].Hour = Summ;
                        SFlights.Add(MyForm.ChoosenFlights[GridSelectedFlights.CurrentRow.Index]);
                        DrawMyFlightTable();
                        MyForm.ChoosenFlights.RemoveAt(GridSelectedFlights.CurrentRow.Index);
                        DrawFlightTable();
                    }
                }
                else
                {
                    SFlights[GridChoosedFlights.CurrentRow.Index].Hour = Summ;
                    DrawMyFlightTable();
                }
                DrawTimeTable();
            }
        }

        private void BtnDeleteFlight_Click(object sender, EventArgs e)
        {
            if (GridSelectedFlights.Enabled)
            {
                MyForm.ChoosenFlights.RemoveAt(GridSelectedFlights.CurrentRow.Index);
                DrawFlightTable();
            }
            else
            {
                SFlights.RemoveAt(GridChoosedFlights.CurrentRow.Index);
                DrawMyFlightTable();
            }
            DrawTimeTable();
        }

        private void BtnToAnotherCity_Click(object sender, EventArgs e)
        {
            if(CheckAvaliable(MyForm.DistanseMatrix[MyForm.MyPlanes[MyForm.GridMyPlanes.CurrentRow.Index].CurrentCity, ComboCityTo.SelectedIndex]))
            {
                Flight tmp = new Flight();
                tmp.Hour = Summ;
                tmp.Dohod = 0;
                tmp.DepartureCity = MyForm.Cities[ComboCityFrom.SelectedIndex];
                tmp.DestinationCity = MyForm.Cities[ComboCityTo.SelectedIndex];
                tmp.Length = MyForm.DistanseMatrix[ComboCityFrom.SelectedIndex, ComboCityTo.SelectedIndex];
                tmp.Costs = MyForm.FuelPrice * 10 * tmp.Length;
                SFlights.Add(tmp);
                DrawMyFlightTable();
                DrawTimeTable();
            }
        }
    }
}
