﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace aeroflot
{
    [Serializable]
    public class BasicPlane
    {
        [XmlAttribute("name")]
        public string Name { get; set; }
        public List<TypePlane> Type;

        public BasicPlane()
        { }

        public BasicPlane(List<TypePlane> _type)
        {
            Type = _type;
        }
    }
}
