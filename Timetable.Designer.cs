﻿namespace aeroflot
{
    partial class Timetable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GridTime = new System.Windows.Forms.DataGridView();
            this.h1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.h2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.h3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.h4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.h5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.h6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridSelectedFlights = new System.Windows.Forms.DataGridView();
            this.BtnCanselTimetable = new System.Windows.Forms.Button();
            this.BtnSelectTime = new System.Windows.Forms.Button();
            this.GridChoosedFlights = new System.Windows.Forms.DataGridView();
            this.ComboDay = new System.Windows.Forms.ComboBox();
            this.ComboHour = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnChangeTable = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lbCity = new System.Windows.Forms.Label();
            this.ComboCityTo = new System.Windows.Forms.ComboBox();
            this.BtnToAnotherCity = new System.Windows.Forms.Button();
            this.BtnDeleteFlight = new System.Windows.Forms.Button();
            this.ComboCityFrom = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.GridTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridSelectedFlights)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridChoosedFlights)).BeginInit();
            this.SuspendLayout();
            // 
            // GridTime
            // 
            this.GridTime.AllowUserToAddRows = false;
            this.GridTime.AllowUserToDeleteRows = false;
            this.GridTime.AllowUserToResizeColumns = false;
            this.GridTime.AllowUserToResizeRows = false;
            this.GridTime.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridTime.ColumnHeadersVisible = false;
            this.GridTime.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.h1,
            this.h2,
            this.h3,
            this.h4,
            this.h5,
            this.h6});
            this.GridTime.Enabled = false;
            this.GridTime.Location = new System.Drawing.Point(410, 58);
            this.GridTime.Name = "GridTime";
            this.GridTime.RowHeadersVisible = false;
            this.GridTime.RowTemplate.Height = 40;
            this.GridTime.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.GridTime.Size = new System.Drawing.Size(630, 372);
            this.GridTime.TabIndex = 0;
            // 
            // h1
            // 
            this.h1.HeaderText = "h1";
            this.h1.Name = "h1";
            // 
            // h2
            // 
            this.h2.HeaderText = "h2";
            this.h2.Name = "h2";
            // 
            // h3
            // 
            this.h3.HeaderText = "h3";
            this.h3.Name = "h3";
            // 
            // h4
            // 
            this.h4.HeaderText = "h4";
            this.h4.Name = "h4";
            // 
            // h5
            // 
            this.h5.HeaderText = "h5";
            this.h5.Name = "h5";
            // 
            // h6
            // 
            this.h6.HeaderText = "h6";
            this.h6.Name = "h6";
            // 
            // GridSelectedFlights
            // 
            this.GridSelectedFlights.AllowUserToAddRows = false;
            this.GridSelectedFlights.AllowUserToDeleteRows = false;
            this.GridSelectedFlights.AllowUserToResizeColumns = false;
            this.GridSelectedFlights.AllowUserToResizeRows = false;
            this.GridSelectedFlights.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridSelectedFlights.Location = new System.Drawing.Point(12, 58);
            this.GridSelectedFlights.Name = "GridSelectedFlights";
            this.GridSelectedFlights.ReadOnly = true;
            this.GridSelectedFlights.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridSelectedFlights.Size = new System.Drawing.Size(392, 187);
            this.GridSelectedFlights.TabIndex = 1;
            // 
            // BtnCanselTimetable
            // 
            this.BtnCanselTimetable.Location = new System.Drawing.Point(12, 446);
            this.BtnCanselTimetable.Name = "BtnCanselTimetable";
            this.BtnCanselTimetable.Size = new System.Drawing.Size(140, 30);
            this.BtnCanselTimetable.TabIndex = 2;
            this.BtnCanselTimetable.Text = "Отмена";
            this.BtnCanselTimetable.UseVisualStyleBackColor = true;
            this.BtnCanselTimetable.Click += new System.EventHandler(this.BtnCanselTimetable_Click);
            // 
            // BtnSelectTime
            // 
            this.BtnSelectTime.Location = new System.Drawing.Point(900, 12);
            this.BtnSelectTime.Name = "BtnSelectTime";
            this.BtnSelectTime.Size = new System.Drawing.Size(140, 30);
            this.BtnSelectTime.TabIndex = 3;
            this.BtnSelectTime.Text = "Назначить";
            this.BtnSelectTime.UseVisualStyleBackColor = true;
            this.BtnSelectTime.Click += new System.EventHandler(this.BtnSelectTime_Click);
            // 
            // GridChoosedFlights
            // 
            this.GridChoosedFlights.AllowUserToAddRows = false;
            this.GridChoosedFlights.AllowUserToDeleteRows = false;
            this.GridChoosedFlights.AllowUserToResizeColumns = false;
            this.GridChoosedFlights.AllowUserToResizeRows = false;
            this.GridChoosedFlights.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridChoosedFlights.Enabled = false;
            this.GridChoosedFlights.Location = new System.Drawing.Point(12, 251);
            this.GridChoosedFlights.Name = "GridChoosedFlights";
            this.GridChoosedFlights.ReadOnly = true;
            this.GridChoosedFlights.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridChoosedFlights.Size = new System.Drawing.Size(392, 179);
            this.GridChoosedFlights.TabIndex = 4;
            // 
            // ComboDay
            // 
            this.ComboDay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboDay.FormattingEnabled = true;
            this.ComboDay.Location = new System.Drawing.Point(226, 21);
            this.ComboDay.Name = "ComboDay";
            this.ComboDay.Size = new System.Drawing.Size(121, 21);
            this.ComboDay.TabIndex = 5;
            this.ComboDay.SelectedIndexChanged += new System.EventHandler(this.ComboDay_SelectedIndexChanged);
            // 
            // ComboHour
            // 
            this.ComboHour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboHour.FormattingEnabled = true;
            this.ComboHour.Location = new System.Drawing.Point(407, 21);
            this.ComboHour.Name = "ComboHour";
            this.ComboHour.Size = new System.Drawing.Size(121, 21);
            this.ComboHour.TabIndex = 6;
            this.ComboHour.SelectedIndexChanged += new System.EventHandler(this.ComboHour_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(353, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "День";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(534, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Час";
            // 
            // BtnChangeTable
            // 
            this.BtnChangeTable.Location = new System.Drawing.Point(264, 446);
            this.BtnChangeTable.Name = "BtnChangeTable";
            this.BtnChangeTable.Size = new System.Drawing.Size(140, 30);
            this.BtnChangeTable.TabIndex = 9;
            this.BtnChangeTable.Text = "Переключить";
            this.BtnChangeTable.UseVisualStyleBackColor = true;
            this.BtnChangeTable.Click += new System.EventHandler(this.BtnChangeTable_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Расположение самолета :";
            // 
            // lbCity
            // 
            this.lbCity.AutoSize = true;
            this.lbCity.Location = new System.Drawing.Point(165, 29);
            this.lbCity.Name = "lbCity";
            this.lbCity.Size = new System.Drawing.Size(0, 13);
            this.lbCity.TabIndex = 11;
            // 
            // ComboCityTo
            // 
            this.ComboCityTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboCityTo.FormattingEnabled = true;
            this.ComboCityTo.Location = new System.Drawing.Point(611, 452);
            this.ComboCityTo.Name = "ComboCityTo";
            this.ComboCityTo.Size = new System.Drawing.Size(121, 21);
            this.ComboCityTo.TabIndex = 12;
            // 
            // BtnToAnotherCity
            // 
            this.BtnToAnotherCity.Location = new System.Drawing.Point(738, 446);
            this.BtnToAnotherCity.Name = "BtnToAnotherCity";
            this.BtnToAnotherCity.Size = new System.Drawing.Size(140, 30);
            this.BtnToAnotherCity.TabIndex = 13;
            this.BtnToAnotherCity.Text = "Перегнать самолет";
            this.BtnToAnotherCity.UseVisualStyleBackColor = true;
            this.BtnToAnotherCity.Click += new System.EventHandler(this.BtnToAnotherCity_Click);
            // 
            // BtnDeleteFlight
            // 
            this.BtnDeleteFlight.Location = new System.Drawing.Point(900, 446);
            this.BtnDeleteFlight.Name = "BtnDeleteFlight";
            this.BtnDeleteFlight.Size = new System.Drawing.Size(140, 30);
            this.BtnDeleteFlight.TabIndex = 14;
            this.BtnDeleteFlight.Text = "Удалить рейс";
            this.BtnDeleteFlight.UseVisualStyleBackColor = true;
            this.BtnDeleteFlight.Click += new System.EventHandler(this.BtnDeleteFlight_Click);
            // 
            // ComboCityFrom
            // 
            this.ComboCityFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboCityFrom.FormattingEnabled = true;
            this.ComboCityFrom.Location = new System.Drawing.Point(456, 452);
            this.ComboCityFrom.Name = "ComboCityFrom";
            this.ComboCityFrom.Size = new System.Drawing.Size(134, 21);
            this.ComboCityFrom.TabIndex = 15;
            // 
            // Timetable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1052, 488);
            this.Controls.Add(this.ComboCityFrom);
            this.Controls.Add(this.BtnDeleteFlight);
            this.Controls.Add(this.BtnToAnotherCity);
            this.Controls.Add(this.ComboCityTo);
            this.Controls.Add(this.lbCity);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BtnChangeTable);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboHour);
            this.Controls.Add(this.ComboDay);
            this.Controls.Add(this.GridChoosedFlights);
            this.Controls.Add(this.BtnSelectTime);
            this.Controls.Add(this.BtnCanselTimetable);
            this.Controls.Add(this.GridSelectedFlights);
            this.Controls.Add(this.GridTime);
            this.Name = "Timetable";
            this.Text = "Timetable";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Timetable_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.GridTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridSelectedFlights)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridChoosedFlights)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView GridTime;
        private System.Windows.Forms.DataGridView GridSelectedFlights;
        private System.Windows.Forms.Button BtnCanselTimetable;
        private System.Windows.Forms.Button BtnSelectTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn h1;
        private System.Windows.Forms.DataGridViewTextBoxColumn h2;
        private System.Windows.Forms.DataGridViewTextBoxColumn h3;
        private System.Windows.Forms.DataGridViewTextBoxColumn h4;
        private System.Windows.Forms.DataGridViewTextBoxColumn h5;
        private System.Windows.Forms.DataGridViewTextBoxColumn h6;
        private System.Windows.Forms.DataGridView GridChoosedFlights;
        private System.Windows.Forms.ComboBox ComboDay;
        private System.Windows.Forms.ComboBox ComboHour;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BtnChangeTable;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label lbCity;
        private System.Windows.Forms.ComboBox ComboCityTo;
        private System.Windows.Forms.Button BtnToAnotherCity;
        private System.Windows.Forms.Button BtnDeleteFlight;
        private System.Windows.Forms.ComboBox ComboCityFrom;
    }
}